# Introduction to Clean Architecture

First of all, I would like to refer everybody to the books "Clean Code" and "Clean Architecture" edited by Robert C. Martin.

Second, I recommend watching the lecture [TDD and Clean Architecture - Use Case Driven Development](https://www.youtube.com/watch?v=IZWLnn2fNko) by Valentina Cupać.
She explains the topic very well and in a way that matches the rational behind this library very closely.

There are further recommended readings such as

-  [Alistair Cockburn - Hexagonal Architecture](http://alistair.cockburn.us/Hexagonal+architecture)
-  [Jeffrey Palmero - The Onion Architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/)
-  [Robert C. Martin - A Little Architecture](http://blog.cleancoder.com/uncle-bob/2016/01/04/ALittleArchitecture.html)
-  [Silas Graffy - Von Schichten zu Ringen – Hexagonale Architekturen erklärt](https://www.maibornwolff.de/blog/von-schichten-zu-ringen-hexagonale-architekturen-erklaert)

## TL;DR

The idea is:

-  Build your business logic without dependencies to any technology.
-  Hide accessing databases, sending messages or calling another service behind abstractions (interfaces).
-  Build adapters for specific technologies that make your abstractions concrete.
-  Marry business logic and adapters to make it a real world application (with side effects and everything).

## The Most Naive Approach to Build an Application

Say we are supposed to build an order service.
For this example assume we are building a REST service.
It offers a REST API that accepts orders.
Especially in the context of REST we call these objects, that are accepted and exposed by the API, "resources".
Resources often are also referred to as data transportation objects (DTO for short).
DTOs are typically represented on the wire as JSON documents.

We could build our imaginary service the naive way, as depicted in the diagram below.
As the diagram suggests, the construction (aka architecture) of this example service is most simple and straight forward.

<img src="naive_architecture.svg"/>

**Some advantages are**:

-  Easy to understand.
   A junior developer can build this without much support by a senior developer aka architect.
-  A first implementation can be done relatively quickly.
-  No additional layers, hence minimum amount of code.
-  The same data classes to represent business data (orders in this example) are used everywhere.
   Hence, we do not have to implement mapping layers.

**Some disadvantages are**:

-  **Representation of business data**
   -  The same representation of business data is used everywhere.
      Hence, changes to the API directly impact the business logic and the persistence layer and vice versa.
      We call this the "blast radius".
      The stronger things are coupled together the bigger the blast radius.
      It's like a domino effect or nuclear fission or instable chemical compounds (aka explosives).
      You touch one small detail and - **boom**.
   -  There is no distinction whether the data is to be sent over the wire, used in computational business logic or whether it is meant to be persisted in some kind of store.
      The data structure might be perfectly suited for use by the web application but may not be an optimal representation for the persistence layer.
      Or, the opposite around, the data class may be structured so it can be persisted in an efficient way but it might be hard for the business logic to work with this representation.
-  **Mixing business logic and technological aspects**
   -  The whole thing is expensive to test.
      It can almost only be tested in an integrated manner.
      This requires a more complex test fixture.
      Integration tests run slow, as there is web and database communication involved.
      Containers need to be started and shut down etc.
      Usually tests are then skipped or reduced to the bare minimum by management in an attempt to save money and keep up developing new features.
      The result is bad quality (which in turn hinders fast development of new features).
   -  Changes in technology libraries may break the business logic.
      Here, again the comparison with explosives suggests itself.

It might be appropriate to construct a service in this naive way in some very simple use-cases, or, for a prove of concept.
In most cases however, where there is more extensive and more complex business logic involved, this simple construction/architecture leads to the usual increase of cost of change over time to the point where decommissioning the service and building it from scratch is the only way forward.

<img src="cost_of_change.svg"/>

## Separating Business Logic and Technical Code

Clean architecture and similar approaches tell us to spend some more effort on organizing and structuring our code.
The general idea is to separate business logic and technical aspects.

<img src="separate_app_and_business_logic.svg"/>

Factoring the business logic out of the REST controller into an own component is not a real gain.
Still the business logic is tied to a specific database or at least a specific category of databases.
Still our API will break when we change the representation in the database.

Clean architecture now tells us how to get rid of those dependencies to specific technologies.
We are achieving this by introducing **abstractions**.
We do not think "database" nor "message broker" or anything technical when programming the business logic.
We will replace all those concrete technologies by abstractions.
And those abstractions will use "Domain Entities" for input and output, not `@Entity`, not `@Document` not `@Jacksonized` or anything tied to a specific technology.

<img src="using_abstractions.svg"/>

-  By using abstractions, we are separating our business logic from technological aspects.
   -  It is absolutely easy to provide mock implementations for the abstractions for the purpose of testing.
   -  Now, our business logic is easier (hence cheaper) to test because it is not required to program a complex test fixture with database and everything.
   -  Tests can run faster as they are purely computational, no network traffic, no database access etc.
   -  Tests are potentially isolated much better.
      There is no state in the database that may interfere with subsequent tests.
-  Also our business logic does not consume REST resources nor JPA entities nor NoSQL documents.
   Instead our business logic only accepts its own Domain Entities.
   -  These Domain Entities are created to reflect our client's business.
   -  These objects are designed to optimally support the business logic.
      We do not need to consider whether those objects can be efficiently stored in the database.
      We do not need to consider whether those objects can be serialized to JSON so that a web application can process them.
   -  Web/API representation is now isolated from business logic and from persistence representation.
      We can change our persistence technology without affecting our API.
   -  But it is not only about decoupling.
      Your abstractions may significantly deviate from the underlaying implementation to your advantage.
      For example, you may have to consume another (REST) service.
      Now you define an abstraction for that service.
      -  This abstraction can be shaped so that it best serves your purpose.
         For example, it may only contain the "stuff" you need, even though the actual service you are consuming might provide much more functionality (which you are not interested in).
      -  You can also strip away data fields you do not need.
         Or convert data fields to another format that better suits your needs.
      -  For the API representation use of enums might be appropriate (`enum InsuranceType{FIRE, LIABILITY, AUTOMOBILE, ...}`).
         In your business code use of polymorphism might be the better approach (`class FireInsurance extends Insurance {}`, `class LiabilityInsurance extends Insurance {}`, `class AutomobileInsurance extends Insurance {}`, ...).

But there is more than just separation of business logic and technical aspects:

-  Make sure that the names of the Domain Entities and their data fields reflect your clients language.
-  Make sure that the names of the Domain Functions also reflect your clients language.
   Don't give your methods generic names such as `process` or `handle`.
   Pick names from your client's language such as `placeOrder` or `cancelOrder`.
   Consider using the language that is used in your country.
   Why introducing additional obstacles by using (potentially wrongly translated) English terms, while your client speaks German, French or what ever language.
-  Having separate class hierarchies for API, businss logic and persistence seems tedious at first.
   However, this gives you two things that cannot be overrated:
   1. It decouples the different layers of your application/service.
      Changes on one layer do not necessarily affect your other layers.
   2. Maybe even more important, you can choose an "optimal" representation for the purpose of the layer.
      Your DTOs (for the API) can be structured as agreed with the developers that are building the web frontend.
      Your Domain Entities can be designed so that the business logic can easily inspect and traverse this data to do its business calculations and checks.
      Your `@Entity`s or `@Document`s can be structured in a way that they can be easily stored and retrived (by an OR-Mapper) from your database.
-  Having different representations for API, business logic and database has yet another justification.
   What if you store things in the database that you do not want to expose to the outside world?

DTOs, Domain Entities, and database entities very much look alike - which is not surprising.
After all, the service does not process orders but stores bananas.
Therefore, developers often think that this distinction is academic and that there is no necessity to distinguish this in code.
DTOs and database entities are just a technical solution predominantly used to transfer data.
Domain Entities on the other hand represent concepts in the business domain.
There is good conceptual and technical reason to make a distiction.

Also note that I am using the term "repository".
"Repository" here means an **idea/concept** of a component which you can use to store and retrieve business data.
This is not to be confused with Spring JPA or Mongo repositories.
Classes to be used with JPA repositories need to be annotated with `@Entity` whereas classes to be used with Mongo repositories need to be annotated with `@Document`.
This is a clear indicator that such classes are tied to one specific technology.
This is what we do not want in our business logic.
Exchanging PostgreSQL for MongoDB should not break our business logic.
The business logic should be something "eternal", something that is always "true" and does not loose value with changing technologies.
Technologies come and go, ideas and concepts stay.

We do not only introduce abstractions for "big things" like databases or other service we want to consume.
When I conduct code reviews, I often find things like `OffsetDateTime.now(MY_ZONE_ID)` or `UUID.randomUUID()` in the middle of the business logic.
Sometimes such things can be found in the constructors of DTOs or entities.
How do you test such code?
How do you test that your business logic still works when switching from summer to winter time?
The answer is: You can't, or, at least it will not be easy.
Therefore, we also see "give me the current date/time" and "give me a UUID" as technical services which we are consuming.
Hence, we are using abstractions for them as well.
When testing, we can provide mocks that we are having under full control and that give us what ever date and time we want for testing.

## Bringing in Technology Again

But how can such business logic, pure computational without ties to concrete technologies have an effect in terms of storing something in a database, sending messages to a broker or answering REST requests?
According to clean architecture we have to provide **adapters**.
These adapters finally bring our abstractions to live by marrying them with well proven technologies such as Spring Repositories, Apache HTTP Client and many more.

<img src="final_architecture.svg"/>

In the diagram above, the business logic is just one box.
Of cause we have to further subdivied our business logic into smaller parts.
There will be "Use Cases", "Repositories", "Domain Entities", "Domain Services" etc.
Each component shall serve only one specific purpose.
Each component shall be easy to test.
Each component shall be easy to understand.
There is a clear call convention: Use Cases are using Domain Services and Domain Services are using Domain Repositories.
A Domain Repository never calls a Use Case.

And, we are not doing this for our persistence layer only.
Every time our business logic is supposed to interact with the outside world, we will come up with an abstraction and an implementation (adapter) later.
Being it database connections, message broker connections or the consumption of other services (REST, GraphQL, RPC, etc.)

By the way, exchanging technologies is a breeze.
The abstraction stays the same.
What changes is the implementation under the hood.
Without impact on the business logic nor on the tests.

Some say that architecture is the sum of the decisions you make.
Others say that you should defer decisions as late as possible.
Working with abstractions makes this possible.
You can start with a mock/in-memory data source and later, when your understanding of the problem and requirements has improved, decide for SQL or NoSQL or whatever persistency technology is appropriate.

## Not Only for Bloated Java Business Applications

Often, developers let me know, that this approach to them seems like over-engineering: _"Maybe this is fine for bloated Java business applications. But it will not work for embedded systems."_
This is definitely not true.
In his book "Test-Driven Development for Embedded C", James W. Grenning demonstrates how this can be applied to embedded development.

I am using this way of constructing software for embedded development with C/C++, too.
For example, I have created a (rather simple) hardware abstraction for my beloved Arduino.
This enables extensive testing, even with timing involved.
