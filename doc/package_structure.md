# Proposed Java Package Structure

Packages can be organized in different ways.
And, discussions on this topic are often - say - passionate.
Therefore we will first discuss two of the most prevalent approaches.
We shall see some of their pros and cons before finally making a recommendation.
The pros and cons listed here are not my own opinion entirely.
It is a summary of several blog posts, forum discussions and text books I have read.

Disclaimer: This "library" also proposes a certain package structure.
This structure is not explained herein in great detail but as part of the accompanying Javadoc.

## Organize by Layer

In this case you may have packages like `com.acme.controller` for the presentation layer, `com.acme.business` for the business logic and `com.acme.persistence` for the database layer.
While all technical components of the same kind are grouped together, the feature related stuff is spread over multiple packages.
This type of organization maybe reveals a rather technical thinking.
(I am definitely guilty of this.)

<img src="organize_by_layer.svg"/>

**Pros**

-  Code of the same kind, like "all controllers", "all JPA repositories" etc. are grouped together.
   Code that is shared by "all Xs" can easily be factored out into shared modules to achieve DRY - don't repeat yourself.
-  Technical application code is separated from functional domain code.
   Especially in big enterprises one may consider providing modules with pure, reusable business code (Domain Entities, Domain Repositories, Domain Services etc.).
   These (well tested) building blocks could be reused in many different applications.
   Wrap them inside a REST controller and you have your synchronous REST API.
   Wrap them in an integration flow, et voila, there you have your asynchronous, message processing application.

**Cons**

-  One feature, or business function stretches across multiple packages.
   It is hard to get an overview over one single feature.
   When you add a new feature to the order service for example, then you have to find involved classes in many different packages/folders.
-  This way of structuring the code has the tendency of creating more abstract classes to achieve reuse.
   Code tends to become more complex, overuse of generics is not seldom.
   Sometimes KISS (keep it simple stupid) is more importand than DRY.
   (Actually, for me, KISS is one of the most important principles in software development.)

## Organize by Feature

In this case you may have packages like `com.acme.order` for everything belonging to "orders" and `com.acme.product` for everything related to "products".
Now you have all your feature related things in one place, controllers, repositories, DTOs, Domain Entities, Domain Services etc.
Changes of technological nature, maybe result of an software update, are now taking place in multiple locations.
This type of organization maybe reveals a rather functional or domain related thinking.

<img src="organize_by_feature.svg"/>

**Pros**

-  Each feature is packaged into a self-contained package.
   It is easier to see, what is making a feature.
   Changes to a single feature take place in a bunch of classes assembled in one place.
-  As we are not aiming at DRY to ambitiously, our code may be not as abstract and complex.

**Cons**

-  Potentially more code is to be writte, even though that code might be simpler.

## Conclusion and Recommendation

As the forum discussions are showing, there are plenty of diffenent opinions, both approaches intoduced above do have their followers.
The decisiton for one of these approaches seems not to be that easy.
The pros and cons, I think, are not as compelling, there is no clear winner.
Therefore, I recomment not dogmatically stick to one of these in any case but pick the one most beneficial for the concrete task at hand.

For example, it is assessed to be an disadvantage that the **organization by layer** spreads feature code, that belongs together, over multiple packages.
(This is true.)
The disadvantage they say, is, that it is hard to find the classes that belong together to constitute a feature.
This is true, if we were browsing the bare file system.
However, modern IDEs provide features that let us quickly find what we are searching for.
For example, our classes may be named `OrderController`, `OrderJpaRepository`, `OrderDomainRepository` etc.
By typing the prefix `order` I can quickly find everything "order".

As compelling the approach of **organizing by feature** sounds, I personally dislike mixing up **application logic** and pure **domain logic**.
For me, a physical service - in terms of a server that listens to network sockets in one or the other way - is just an arbitrary implementation to make business logic available on the net (for distributed computing).
So, services and application logic to me is something exchangable, domain logic is not.

Another thing we should take into account: We do not build huge monoliths anymore.
Nowerdays, we are building micro services most of the time.
A microservice has, as the name suggests, a clearly restricted scope.
This does not only imply, that a micro service consists of less code compared to a monolith.
It also means, that a micro service does not expose business logic from different domains.
Micro services are providing business logic from a single domain (single sub-domain from a domain most likely.)
A micro service can still consist of many components.
I think, it is more significant that a micro service focuses on a single domain (limited functionality) rather than shere code size when it comes to defining the term "micro service".
<br>
Most of the code sits in the business logic.
The least part of the code sits in the technical layer.
The latter is thanks to technical frameworks that liberate us from a lot of details we had to care for in the past.
Database access has become a breeze thanks to Spring JPA repositories and JDBC templates.
Providing REST APIs has become so easy thanks to openAPI code generation, REST controllers and REST templates.
Furthermore, I expect a microservice to implement logic from **one domain only**.
A service might consume services from other domains, but the service itself belongs only to one domain and is therefore the master of that domain's data.

My personal approach is as follows. On the top level, I distinguish:
- Package `application`
    - Contains JPA repositories, REST controllers, integration flows, database entities etc.
      All the technical code that makes the physical service and which constitutes the application.
      That also includes logging, metrics, security and other cross cutting concerns.
    - This is a relatively thin layer, thanks to the use of technical frameworks for building REST APIs, accessing databases, emmitting metrics etc.
    - Depending on the extend of the application:
        - If the application is small, I put all technical classes here.
        - If the application is big, then I introduce further sub-packages:
            - `api`, `messaging`, `persistence`, ...
- Package `domain`
    - Contains Domain Repositories, Domain Entities, Domain Services etc.
      All the business logic that is universal and reusable throughout the whole company.
    - This contains most of the code.
      No framework is helping us building our client's custom logic.
      Furthermore, this is what really distinguishes the services from each other.
    - By the way, the domain logic might even be developed independent of concrete applications.
      One can then various modules of domain logic to produce a concrete service/application.
      The domain logic can (potentially) be co-deployed in many applications (as far as they are built with Java or what ever language you chose for your domain logic).
    - Depending on the extend of the domain logic:
        - If the domain logic is small, I put all domain classes here.
        - If the domain logic is big, then I introduce further sub-packages:
            - `entity`, `repository`, `service`, `usecase`...
    - As a service belongs to one domain, I usually do not introduce different domain packages.
    - If the service consumes other services, then these are just additional Domain Repositories, which are just abstractions (interfaces).
    - Those other services may be from other domains.
      However, we are not using that other domains Domain Entities.
      Under the hood, the implementation of the Domain Repository (adapter), communicates with that other sevice according to a contract defined by that other service.
      Often such contract is given in form of an openAPI, XML schema, WSDL or IDL document.
      On the wire we often communicate JSON or XML.
      Our Domain Repository (facade for the external service) defines its own Domain Entities.
      We are decoupling ourselfes from the external service.
      Say, we are building a collections and disbursement service in the payments sub-domain of an insurance domain.
      Therefore, our service needs to consume insurance contracts from the policy management sub-domain of the insurance domain.
      However on our side, the payments, we are not using the Contract Domain Entities.
      We are using our **view** on Contract Domain Entities that just contains the data we need for collections and disbursements.
      If the number of classes grow, then it may make sense to indroduce sub-packes for our own and foreign domains.

Please take a look at the accomanying Javadoc.
I have included possible packages in the source code.
The Javadoc contains explanations for what I think should be located in that packages.

What ever approach you decide for, the important thing is, that everybody on the team needs to know and understand the decided way of organizing packages.
And, of cause, everybody needs to follow it, once the decision is made.
However, flexibility is more important than sticking with decitions made way back.
If you feel that the chosen package organization does not support your efforts, then change it.
