# Proposed Naming Convention

## Naming Things

Herein I am defining some names and their semantics.
These names are used in the documentation but also in the code.

Herin only rough overview is given.
Names and semantics are documented in detail in the accompanying Javadoc.

| Name              | Annotation             | Semantics |
|-------------------|------------------------|-----------|
| Domain Entity     | `@DomainEntity`        | Data structures to model real world objects from the business domain. Examples are "account" for the banking domain or "order" for the retail domain. |
| Aggregate         | `@Aggregate`           | Some Entities are Aggregates, see Domain Driven Design by Eric Evans. |
| Domain Type       | `@DomainType`          | Primitive types such as `FirstName`, `Amount` etc. to improve type safety. |
| Domain Repository | `@DomainRepository`    | Meant for storing and retrieving Domain Entities. Typically implemented by use of Spring Data repositories but also REST or messaging clients. |
| Domain Function   | `@DomainFunction`      | A simple piece of (helper) business logic that can be implemented as a pure function. Easy to test. Domain Functions can be used by Domain Services to build more complex logic. |
| Domain Service    | `@DomainService`       | Universally applicable business logic. Uses Domain Repositories. Usually requires mocking for testing. |
| Use Case          | `@UseCase`             | An more complex piece of business logic. May use multiple Domain Services to support complete application Use Cases. |
| Transaction       | `@BusinessTransaction` | Binds operations together into one transaction. Transaction control is located at Use Case level. |
| Common Component  | `@CommonComponent    ` | A component that is so universal that it is shared by application and domain code. (Should be rare though.) |

Why do I use the terms **Domain** Entity and **Domain** Repositiory?
Why not just calling them Entity and Repository?
There are two reasons:

1. I want to stress the fact that these abstractions represent concepts from the problem domain.
   Domain Entities are of cause coded, hence they are something technical.
   But technology is not the central thing about them.
2. I want to disringuish them from other well known software components like Spring's `@Repository`s and `@Entity`s.
   `@Repository` and `@Entity` are purely technical things.
   They are making our lives much easier when we need to store things in a database.

## What's the Meaning?

Names are just "handles" to things.
What do these things mean?
Let's take a look at the diagram below.

The diagram shows two regions:
-  **Application Logic & Technologies**
    -  Implements logic that is specific to a certain application.
    -  Implements logic for accessing certain technologies.
       Most of this stuff is implementations (adapters) of abstractions (aka interfaces) defined by the domain logic.
    -  The technologies used here might come in a wide variety.
       Frameworks like Spring Data, Spring Integration, Apache HTTP Client, Retrofit and many more help us in this area.
    -  The application/technology ring could be more strictly subdivided into "application" and "technologies".
    -  The application/technology ring is not in the focus of our discussion.
       Herein we focus on domain logic.
    -  Cross cutting concerns sich as logging, monitoring, security and error handling belong here.
       I like to use aspect oriented programming for weaving in cross cutting concerns in order not to blur the actual application logic.
-  **Domain Logic**
    -  Implements generic domain logic that might not only be used by this specific application.
       The domain logic might in fact reside in reusable domain logic libraries.
    -  The logic contained herein is logic from **our** domain.
       We might actually need to consume data from other domains.
       Or, we might have to use functionality from other domains.
       We will not implement or reuse logic from other domains however.
       Instead we build facades to those other domains that suit our needs.
       An implementation of such a facade might for example be a REST client that talks to a REST API of that other domain.
       The facade does not need to map 1:1 to the other domain.
       Within some degree of freedom we can shape it that it fits our need.
       For example, we may choose to omit data fields which we do not require (yet).
       Important: All data exchange between applications and services should be in accordance with an agreed upon integration data model.
       Never should we directly expose our Domain Objects, and so shouldn't others.

At the interface between application/technology and domain logic, we need to map data structures.
For example a REST controller uses REST resources (defined in an openAPI document) where the domain logic uses Domain Entities.
The REST controller needs to map resources to Domain Objects when invoking the domain logic.
And, on the way back, the resulting Domain Entities need to be mapped to resources that can be sent back to the web application (most likely as a JSON document).
Again, technology frameworks such as MapStruct, Dozer or ModelMapper help us with this task, if we want.

<img src="domain_architecture.svg"/>

-  **Use Case**
    -  Use Cases accept Use Case Requests as input and return Use Case Responses as output.
       Both are envelope datastructures that are holding domain objects.
    -  Use Cases may orchestrate multiple calls to multiple Domain Services to achieve a complex/composed piece of application logic.
    -  Use Cases might be part of the domain logic, if they are generally reusable.
       Often Use Cases better reside in the application logic package, as they are often strongly tied to a specific application.
    -  Use Cases have enough "knowledge" to sensibly span and manage transactions transactions.
       In the worst case transactions might be **distributed transactions**.
       In this case I would prefer to use an XA framework as things can become complicated very quickly.
       By the way, this framework contains annotations and classes for transaction management.
       Under the hood these are mapped to corresponding Spring implementations.
       Again, we can use and test transactions in an technology agnostif fashion.
    -  Use Cases hold references to Domain Services.
       These are Injected by Spring for example.
       However, the Use Case is not aware of the actual implementation.
       Rather it knows the interface and is programmed against this interface.
-  **Domain Service**
    -  These are collections of generic (hence reusable) units of domain logic.
    -  Domain Services may use much simpler Domain Functions to realize their functionality.
    -  Domain Services only know Domain Entities for input and output.
    -  Domain Services may interact with their environmens, for example with databases and other services.
       Database access is achieved by means of Domain Repositories as we shall see in a minute.
       Access to other services is different.
       In this case we do not implement a Domain Repository but use for example REST templates instead.
       The important point is, that the abstraction only uses Domain Objects and is technology agnostic.
       It is the obligation of the concrete implementation to map Domain Objects to REST resources, orchestrate a REST template etc
    - Use your client's language.
-  **Domain Function**
    -  Super simple functions to do business computations.
    -  Not as complex as Domain Services.
       For example, does not have any beans it depends upon.
    -  Pure functional.
       Only depends on the input given.
    -  The idea here is, that code (especially when reusable) can be pulled out of Domain Services to make them simpler.
    -  Examples could be predicate functions and projections.
-  **Domain Repositories**
    -  Domain Repositories are abstract stores for Domain Entities.
    -  They are completely application agnostic.
       As a result we
        -  gain tesability and
        -  can defer decisions on actual persistence technology and low level representation until later.
    -  Domain Services are holding references to Domain Repositories.
       The Domain Repositiries are injected by Spring.
       The Domain Service however only knows the interface and does not care for specific implementations and persistence technologies.
-  **Domain Entities**
    -  Datastructures that are modeled and named to follow our client's thinking, naming and language.
    -  Designed to support the business logic, not the database.
       For the web, for messaging, for persistence we use a separate, technical representation that supports communication via web or message broker, or which  can be easily OR-mapped to database tables.
    - Use your client's language.
-  **Domain Types**
    - Primitive types wrapped in a dedicated type to benefit from compile time type checking.
