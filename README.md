# CASL4J - The Clean Architecture Support Library for Java

Library and aids that support your efforts to build Java applications that follow the "Clean Architecture" approach.
However, "Clean Architecture" here does not only refer to the book edited by Robert C. Martin.
In a broader sense this project addresses "well constructed" software in general - software that is easy to test, easy to understand and that can be maintained indefinitely.

2021-02-08, Holger Zahnleiter

## Introduction

It seems challenging to understand the approach of clean architecture when you are exposed to it the first time - especially for junior developers.
Building an actual application that follows that approach as close as possible is even more challenging when your are doing it the first time.
Therefore, I have created this project to support your efforts in building clean applications.

This project is not tied to any specific dependency injection (DI) framework, nor to any specific database or messaging technology.
In fact, all code contained herein is pure Java with no external dependencies to any 3rd party libraries or frameworks.
This library actually does not deliver much code.
It is more of a proposal than an actual library and consists mostly of:

-  An [introduction](doc/introduction_to_clean_architecture.md) to clean architecture.
-  A proposed [naming convention](doc/naming_convention.md) and associated semantics, that borrows from ideas promoted by hexagonal, onion and clean architecture.
   Examples for such conceps are: "Domain Entity", "Repository", "Use Case" and others.
-  Annotations to mark and identify classes and functions as being a "Domain Entity", a "Repository", a "Use Case" etc.
-  A proposal for a Java [package structure](doc/package_structure.md) also inspired by aforementioned architectural concepts.

Please be advised that [introduction](doc/introduction_to_clean_architecture.md), [proposed naming convention](doc/naming_convention.md) and [proposed package structure](doc/package_structure.md) are not described super extensively.
Instead, everything can be found in the **Javadoc** of the [source code](src/main/java/org/zahnleiter/casl4j).
Every annotation, each package, every class povides extensive documentation to explain its meaning and the ideas behind.

Furthermore, this project actually comes in multiple Git repositories:

-  [This repository](https://gitlab.com/hzahnlei/casl4j), delivering annotations, helpers and proposals for building well architected applications and services.
-  [Architecture checks](https://gitlab.com/hzahnlei/casl4j-archcheck), you can use for checking whether your code is compliant to clean architecture or not.
-  [Extension for Spring](https://gitlab.com/hzahnlei/casl4j-spring), that marries CASL4J with the Spring framework.
   Lets you build cleanly architected applications with the Spring framework.
   All your "Use Cases" and "Repositories" etc. automatically become Spring beans.
-  [Spring demo](https://gitlab.com/hzahnlei/casl4j-spring-demo), a cleanly architected REST service build with CASL4J and the Spring framework.

By the way, one could also provide implementations of CASL4J for Micronaut or Quarkus.

## How to Add this Library to Your Project

### Cloning and Downloading

Of cause you can clone this project, compile it and push the artifacts into your local Maven repository and use it from there.

Furtheremore, the artifacts can be downloaded from GitLab.
Do so and add the JAR to your project's class-path.

### Maven Repository

The preferred way is to add the artifacts (JAR, source and Javadoc) to your project as Maven or Gradle dependencies.
This requires that you first add my GitLab Maven repository to your Gradle or Maven build file like so:

```groovy
repositories {
    ...
    maven {
        url 'https://gitlab.com/api/v4/projects/36505468/packages/maven'
        name 'casl4j-gitlab-registry'
    }
    ...
}
```

Then, add a dependency like shown next:

```groovy
dependencies {
    ...
    implementation 'org.zahnleiter:casl4j:1.0.0'
    ...
}
```

## Disclaimer

This is a private, free, open source and non-profit project (see LINCENSE file).
Use on your own risk.
I am not liable for any damage caused.

I am a private person, not associated with any companies mentioned herein.

## Credits

The ideas presented herein are not my own.
I am building on the ideas and experience of countless software developers and software architects before me.
Also, I am using many open source and free software and services.
Thank you:

-  [GitLab](https://gitlab.com)
-  [Visual Studio Code](https://code.visualstudio.com) - Microsoft
-  [Eclipse](https://www.eclipse.org/downloads/) - Eclipse Foundation
-  [Git](https://git-scm.com)
-  [Gradle](https://gradle.org)
-  [Maven](https://maven.apache.org) - Apache Software Foundation
-  Many more that I might have forgotten or even not be aware of using...

## Technical Note on Eclipse/Spring Tools for Eclipse and Gradle

I have experienced a problem that (not only) this project could not be built inside the Spring Tools version of Eclipse.
This is for the following reason:

-  Spring Tools for Eclipse comes bundled with its own JDK (JDK 17 in my case).
-  However, Gradle (Buildship plug-in) uses another JDK that is installed on the machine.
-  Therefore, under SringToolSuite4/Properties.../Gradle set the Java home to match the one used by Spring Tools for Eclipse.
   In this example this is `/Applications/SpringToolSuite4.app/Contents/Eclipse/plugins/org.eclipse.justj.openjdk.hotspot.jre.full.macosx.x86_64_17.0.2.v20220201-1208/jre` - the JDK Spring Tools are bundled with.
   Of cause, on your operating system or your individual installation this may differ greatly.
-  Now Eclipse/Spring Tools and Gradle plug-in are using the same JDK.
   Bytecode version problems are gone.

Also I am experiencing problems importing Gradle applications in Eclipse in general.
What helps me here is to execute `./gradlew assemble eclipse` on the command line and then import the project, or refresh Gradle project, when imported already.
