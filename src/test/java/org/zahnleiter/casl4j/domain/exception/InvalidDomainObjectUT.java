package org.zahnleiter.casl4j.domain.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.UUID;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InvalidDomainObjectUT {

    @Test
    void Construction_creates_meaningful_message() {
        final var id = UUID.randomUUID();
        final var time = OffsetDateTime.now(ZoneId.of("UTC"));
        final var exception = new InvalidDomainEntity("Blub!", id, time);
        assertEquals("Invalid domain entity. Blub!", exception.getMessage());
        assertEquals(id, exception.id);
        assertEquals(time, exception.timeOfOccurrence);
    }

}
