package org.zahnleiter.casl4j.domain.transaction;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.domain.usecase.AbstractBusinessTransaction;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AbstractBusinessTransactionUT {

    public static class TestableAbstractBusinessTransaction extends AbstractBusinessTransaction {

        public static boolean doCommitHasBeenInvoked = false;

        @Override
        protected void doCommit() {
            doCommitHasBeenInvoked = true;
        }

        public static boolean doRollbackHasBeenInvoked = false;

        @Override
        protected void doRollback() {
            doRollbackHasBeenInvoked = true;
        }

        @Override
        public boolean needsCompletion() {
            return !doCommitHasBeenInvoked && !doRollbackHasBeenInvoked;
        }

        public static void reset() {
            doCommitHasBeenInvoked = false;
            doRollbackHasBeenInvoked = false;
        }

    }

    @BeforeEach
    void resetTestProbes() {
        TestableAbstractBusinessTransaction.reset();
    }

    @Test
    void Transaction_rolls_back_automatically_if_not_committed() throws Exception {
        try (final var transaction = new TestableAbstractBusinessTransaction()) {
            assertTrue(transaction.needsCompletion());
        }
        assertFalse(TestableAbstractBusinessTransaction.doCommitHasBeenInvoked);
        assertTrue(TestableAbstractBusinessTransaction.doRollbackHasBeenInvoked);
    }

    @Test
    void Transaction_rolls_back_automatically_on_exception() throws Exception {
        try (final var transaction = new TestableAbstractBusinessTransaction()) {
            assertTrue(transaction.needsCompletion());
            throw new NullPointerException();
        } catch (final NullPointerException cause) {
            // Intentionally left blank
        }
        assertFalse(TestableAbstractBusinessTransaction.doCommitHasBeenInvoked);
        assertTrue(TestableAbstractBusinessTransaction.doRollbackHasBeenInvoked);
    }

    @Test
    void Transaction_has_to_be_committed_explicitly() throws Exception {
        try (final var transaction = new TestableAbstractBusinessTransaction()) {
            assertTrue(transaction.needsCompletion());
            transaction.commit();
            assertFalse(transaction.needsCompletion());
        }
        assertTrue(TestableAbstractBusinessTransaction.doCommitHasBeenInvoked);
        assertFalse(TestableAbstractBusinessTransaction.doRollbackHasBeenInvoked);
    }

}
