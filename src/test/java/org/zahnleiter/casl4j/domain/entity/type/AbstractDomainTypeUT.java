package org.zahnleiter.casl4j.domain.entity.type;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.domain.exception.InvalidValueForDomainType;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AbstractDomainTypeUT {

    static final class TestableAbstractDomainType
            extends AbstractDomainType<Integer> {

        protected TestableAbstractDomainType(Integer value) {
            super(value);
        }

        @Override
        protected void assertIsValid(Integer value) throws InvalidValueForDomainType {
            if (value < 0) {
                throw new InvalidValueForDomainType("Must not be negative", UUID.randomUUID(), OffsetDateTime.now());
            }
        }
    }

    @Test
    void Can_instantiate_domain_type_with_valid_value() {
        assertDoesNotThrow(() -> new TestableAbstractDomainType(+10));
    }

    @Test
    void Cannot_instantiate_domain_type_with_invalid_value() {
        try {
            new TestableAbstractDomainType(-10);
            fail("Expected exception");
        } catch (InvalidValueForDomainType cause) {
            assertEquals("Invalid field value. Must not be negative", cause.getMessage());
        }
    }

}
