package org.zahnleiter.casl4j.domain.entity;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.zahnleiter.casl4j.domain.exception.InvalidDomainEntity;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AbstractDomainEntityUT {

    static final class TestableAbstractDomainEntity
            extends AbstractDomainEntity {

        public final int count;

        protected TestableAbstractDomainEntity(int count) {
            this.count = count;
            assertIsValid();
        }

        @Override
        protected void assertIsValid() throws InvalidDomainEntity {
            if (count < 0) {
                throw new InvalidDomainEntity("Must not be negative", UUID.randomUUID(), OffsetDateTime.now());
            }
        }
    }

    @Test
    void Can_instantiate_domain_entity_with_valid_value() {
        assertDoesNotThrow(() -> new TestableAbstractDomainEntity(+10));
    }

    @Test
    void Cannot_instantiate_domain_type_with_invalid_value() {
        try {
            new TestableAbstractDomainEntity(-10);
            fail("Expected exception");
        } catch (InvalidDomainEntity cause) {
            assertEquals("Invalid domain entity. Must not be negative", cause.getMessage());
        }
    }

}
