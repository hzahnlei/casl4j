package org.zahnleiter.casl4j.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZoneId;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class OffsetDateTimeFactoryUT {

    @Test
    void Default_constructed_factory_creates_UTC_time_stamps() {
        final var factory = new OffsetDateTimeFactory();
        final var timestamp = factory.now();
        assertEquals("Z", timestamp.getOffset().normalized().toString());
    }

    @Test
    void Custom_constructed_factory_creates_time_stamps_in_any_time_zone() {
        final var factory = new OffsetDateTimeFactory(ZoneId.of("Europe/Berlin"));
        final var timestamp = factory.now();
        final var actualTimeZone = timestamp.getOffset().normalized().toString();
        assertTrue("+01:00".contentEquals(actualTimeZone) || "+02:00".contentEquals(actualTimeZone));
    }

}
