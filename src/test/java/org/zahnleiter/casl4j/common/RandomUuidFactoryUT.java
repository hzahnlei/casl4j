package org.zahnleiter.casl4j.common;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class RandomUuidFactoryUT {

    @Test
    void Random_UUDI_factory_produces_random_UUIDs() {
        final var factory = new RandomUuidFactory();
        final var uuid1 = factory.newUUID();
        assertNotNull(uuid1);
        final var uuid2 = factory.newUUID();
        assertNotNull(uuid2);
        assertNotEquals(uuid1, uuid2);
    }

}
