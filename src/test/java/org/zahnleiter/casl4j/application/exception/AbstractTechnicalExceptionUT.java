package org.zahnleiter.casl4j.application.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("UT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class AbstractTechnicalExceptionUT {

    @Test
    void Technical_exceptions_have_to_have_a_message_a_unique_ID_and_a_timestamp() {
        final var actualMessage = "Blub!";
        final var actualUuid = UUID.fromString("10000000-2000-3000-4000-500000000000");
        final var actualTimestamp = OffsetDateTime.MIN;
        final var exception = new AbstractTechnicalException(actualMessage, actualUuid, actualTimestamp) {
            private static final long serialVersionUID = 1L;
        };
        assertEquals(actualMessage, exception.getMessage());
        assertEquals(actualUuid, exception.id);
        assertEquals(actualTimestamp, exception.timeOfOccurrence);
    }

    @Test
    void Technical_exceptions_may_also_have_a_root_cause() {
        final var actualMessage = "Blub!";
        final var actualCause = new NullPointerException();
        final var actualUuid = UUID.fromString("10000000-2000-3000-4000-500000000000");
        final var actualTimestamp = OffsetDateTime.MIN;
        final var exception = new AbstractTechnicalException(actualMessage, actualCause, actualUuid, actualTimestamp) {
            private static final long serialVersionUID = 1L;
        };
        assertEquals(actualMessage, exception.getMessage());
        assertEquals(actualCause, exception.getCause());
        assertEquals(actualUuid, exception.id);
        assertEquals(actualTimestamp, exception.timeOfOccurrence);
    }

}
