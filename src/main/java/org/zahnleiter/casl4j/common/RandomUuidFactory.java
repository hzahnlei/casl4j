package org.zahnleiter.casl4j.common;

import java.util.UUID;

/**
 * A concrete factory that produces random UUIDs.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@CommonComponent
public final class RandomUuidFactory implements UuidFactory {

    @Override
    public UUID newUUID() {
        return UUID.randomUUID();
    }

}
