package org.zahnleiter.casl4j.common;

import java.time.LocalTime;
import java.time.OffsetDateTime;

/**
 * Programs should not directly call for example {@link OffsetDateTime#now()}.
 * This cannot easily be tested. Instead, services should depend on a date/time
 * service. For testing mock date/time services can be used for producing tests
 * that are independent of the actual time.
 * <p>
 * This service is generic and will work for {@link OffsetDateTime},
 * {@link LocalTime} etc.
 * <p>
 * This service is a {@link FunctionalInterface}. In the most simple case mocks
 * can be simple lambdas such as
 * {@code () -> OffsetDateTime.parse("2021-02-08T15:24:00Z")}.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface TimeFactory<DT> {

    /**
     * @return The current date/time. Type depends on generic parameter. Timzone (if
     *         any) depends on initialisation or type of factory.
     */
    DT now();

}
