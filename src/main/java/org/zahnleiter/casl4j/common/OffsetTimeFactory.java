package org.zahnleiter.casl4j.common;

import java.time.OffsetDateTime;

/**
 * This is Java's bloatish way to express a type alias.
 * Instead of writing {@code TimeFactory<OffsetDateTime>}, we can now write
 * {@code OffsetTimeFactory}.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface OffsetTimeFactory extends TimeFactory<OffsetDateTime> {
}
