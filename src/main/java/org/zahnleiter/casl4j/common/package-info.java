/**
 * Here we place helper stuff that might be used in both application and domain.
 * However, not much is expected to be found herein in projects that use this
 * library. Typically code can be clearly distinguished to be either application
 * or domain.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.common;