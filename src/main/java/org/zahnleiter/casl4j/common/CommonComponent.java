package org.zahnleiter.casl4j.common;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * In some (rare) cases (utility) code might be shared between (functional)
 * business logic and (technical) application code.
 * Use this annotation to mark such classes as being shared by both parts of the
 * application.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface CommonComponent {
}
