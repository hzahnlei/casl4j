package org.zahnleiter.casl4j.common;

import java.util.UUID;

/**
 * Programs should not directly call {@link UUID}. This cannot easily be tested.
 * Instead, services should depend on a date/time service. For testing mock
 * date/time services can be used for producing tests that are independent of
 * the actual time.
 * <p>
 * This service is a {@link FunctionalInterface}. In the most simple case mocks
 * can be simple lambdas such as {@code () -> UUID.randomUUID()}.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface UuidFactory {

    /**
     * @return A new UUID.
     */
    UUID newUUID();

}
