package org.zahnleiter.casl4j.common;

import java.time.OffsetDateTime;
import java.time.ZoneId;

/**
 * A concrete factory that produces offset date/time objects for given time
 * zones.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 *
 * @see TimeFactory
 */
@CommonComponent
public final class OffsetDateTimeFactory implements OffsetTimeFactory {

    private final ZoneId zoneId;

    /**
     * Default constructor picks UTC as the time zone for all timestamps produced by
     * this factory.
     */
    public OffsetDateTimeFactory() {
        this(ZoneId.of("UTC"));
    }

    /**
     * @param zoneId Pick a time zone that suits your needs.
     */
    public OffsetDateTimeFactory(final ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public OffsetDateTime now() {
        return OffsetDateTime.now(this.zoneId);
    }

}
