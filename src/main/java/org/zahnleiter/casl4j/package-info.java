/**
 * <pre>
 *  #####    ###    #####  ##           ##      ##
 * ##   ##  ## ##  ##   ## ##          ##       ##
 * ##      ##   ## ##      ##          ##       ##
 * ##      #######  #####  ##         ##        ##
 * ##      ##   ##      ## ##        ##         ##
 * ##   ## ##   ## ##   ## ##        ##    ##   ##
 *  #####  ##   ##  #####  #######  ##      #####
 *
 * Clean Architecture Support Library for Java
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * </pre>
 *
 * Below here we basically hold the following:
 * <ol>
 * <li>Package {@code application} - Application specific/technical code.</li>
 * <li>Package {@code domain} - Universally applicable and therefore reusable
 * business logic, non-technical.</li>
 * <li>Package {@code common} - Utillity classes that might be used by both,
 * application and business logic.</li>
 * <li>The application itself, for example a
 * {@code @SpringBootApplication}.</li>
 * </ol>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j;