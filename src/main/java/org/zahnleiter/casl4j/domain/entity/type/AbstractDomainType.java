package org.zahnleiter.casl4j.domain.entity.type;

import org.zahnleiter.casl4j.domain.exception.InvalidValueForDomainType;

/**
 * Basis for primitive Domain Types with an validation method baked in.
 * You do not have to derive from this one though. We can also use bean
 * vaidation or just external funtions for validation.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DomainType
public abstract class AbstractDomainType<T> {

    /**
     * Actual value wrapped by this class. Type depends on generic parameter.
     */
    public final T value;

    /**
     * Validate given value. If valid, then this opbject gets initialized. Otherwise
     * constructor will throw {@link InvalidValueForDomainType}.
     *
     * @param value Value to be wrapped by this Domain Type.
     */
    protected AbstractDomainType(final T value) {
        assertIsValid(value);
        this.value = value;
    }

    /**
     * @param value Value to be checked.
     * @throws InvalidValueForDomainType If {@link #value} not valid for this Domain
     *                                   Type.
     */
    protected abstract void assertIsValid(T value) throws InvalidValueForDomainType;

}
