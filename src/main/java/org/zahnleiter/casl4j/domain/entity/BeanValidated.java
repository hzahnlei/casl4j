package org.zahnleiter.casl4j.domain.entity;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Supports JSR 380 Bean Validation in the same fashion as Spring's
 * {@code @Validated} annotation. However, this annotation is not tied to the
 * Spring framework but technology agnostic. Intended to be used in the domain
 * (business) logic. On the technological layers you can (and should) still use
 * {@code @Validated}.
 * <p>
 * Use the {@code @Valid} annotation on method arguments and return types to
 * have them validated. Use {@code @NotNull}, {@code @Max} etc. to define what
 * is valid.
 * <p>
 * <b>Defining a domain object with JSR 380 validation annotations</b>
 *
 * <pre>
 * public class PersonExampleBean {
 *
 *     &#64;NotNull
 *     &#64;Size(min = 1, max = 40, message = "Name must be between 10 and 40 characters")
 *     public final String name;
 *
 *     &#64;PositiveOrZero(message = "Age must not be negative")
 *     public final int age;
 *
 * }
 * </pre>
 *
 * <b>Applying bean validation in domain logic (technology agnostic)</b>
 *
 * <pre>
 * &#64;BeanValidated
 * public class PersonExampleDomainLogic {
 *
 *     public void add(&#64;Valid final PersonExampleBean person) {
 *         ...
 *     }
 *
 * }
 * </pre>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface BeanValidated {
}
