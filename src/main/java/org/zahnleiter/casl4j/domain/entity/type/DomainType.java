package org.zahnleiter.casl4j.domain.entity.type;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.zahnleiter.casl4j.domain.entity.DomainEntity;

/**
 * Domain Types are simple types. Instead of using primitive types such as
 * {@link java.lang.String} or {@link java.lang.Integer} to build our
 * {@link DomainEntity}s from we are using
 * {@link org.zahnleiter.casl4j.domain.entity.type.DomainType}s. Doing so we
 * achieve type safety.
 * <p>
 * Examples for Domain Types are
 * <ul>
 * <li>{@code ZipCode} - A string of a certain length made up of digits
 * only.</li>
 * <li>{@code Total} - Basically an integer that represents amounts of money.
 * </ul>
 * I would say use of Domain Types is optional, as there is some effort
 * involved. However, I recomend using them as their use improves type safety.
 * {@code firstName} and {@code lastName} for example can easily be confused if
 * both are of type {@link java.lang.String}. This is especially true when
 * passing values to constructors and functions. However, if {@code firstName}
 * and* {@code lastName} have dedicated types, then they cannot be confused as
 * we are getting help from the compiler.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
public @interface DomainType {
}
