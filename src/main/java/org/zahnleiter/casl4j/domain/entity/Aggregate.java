package org.zahnleiter.casl4j.domain.entity;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * {@link DomainEntity}s can be additionally annotated with {@link Aggregate} to
 * indicate an object is an Aggregate in the sense of of domain driven design
 * (DDD).
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(TYPE)
public @interface Aggregate {
}
