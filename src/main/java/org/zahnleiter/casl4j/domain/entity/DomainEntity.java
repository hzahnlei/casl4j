package org.zahnleiter.casl4j.domain.entity;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Domain Entities are data types that correspond to the real world objects the
 * business side is dealing with. Examples are: Contract, order, line item,
 * bank account, delivery address etc. Domain Entities are complex objects
 * composed of multiple fields.
 * <br>
 * "Entity" here does not mean a Spring {@code @Entity}. Rather it refers to the
 * datastructures that model the domain's real-world objects.
 * <p>
 * The names of the Domain Entities should match the language of the business
 * side. For non-English speaking countries it is perfectly OK to use local
 * language for naming those Domain Entities. Examples for German language could
 * be: Vertrag, Bestellung, Bestellposition, Bankkonto, Lieferadresse etc.
 * <p>
 * Furthermore, the Domain Entities should be easy to work with in the business
 * logic. The Domain Entities should be constructed without REST APIs and
 * databases in mind. The Domain Entities should be purely functional and
 * represent the business and the language of the business.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(TYPE)
public @interface DomainEntity {
}
