package org.zahnleiter.casl4j.domain.entity;

import org.zahnleiter.casl4j.domain.exception.InvalidDomainEntity;

/**
 * Basis for complex Domain Entities with an validation method baked in.
 * You do not have to derive from this one though. We can also use bean
 * vaidation or just external funtions for validation.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DomainEntity
public abstract class AbstractDomainEntity {

    /**
     * If a Domain Entity decides to derive from this base class, then its
     * constructor should call this method as its last action.
     * Has no effect if Domain Entity has been initialized correctly.
     *
     * @throws InvalidDomainEntity if Domain Entity not initialized correctly.
     */
    protected abstract void assertIsValid() throws InvalidDomainEntity;

}
