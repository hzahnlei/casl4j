package org.zahnleiter.casl4j.domain.service;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Domain Services are more complex than simple Domain Functions. For example, a
 * Domain Service likely uses Domain Repositories to consume additional data
 * from a database. A Domain Service is very likely to produce side-effects or
 * to be affected by them.
 * <p>
 * Domain Services interact with their environment strictly by use of Domain
 * Repositories. No technology will be baked into them as this destroys
 * testability.
 * <p>
 * In opposite to Domain Functions, Domain Services are harder to unit test.
 * More likely they require mocking and extensive component testing to ensure
 * correct business logic.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(TYPE)
@Documented
public @interface DomainService {
}
