package org.zahnleiter.casl4j.domain.service.support;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Domain Functions are simple and pure functions that perform domain related
 * computations. Being pure implies those functions do not depend on any
 * side-effects nor will the produce any side-effects. The computation is solely
 * performed on the given input (arguments) alone. No additional data will be
 * read etc.
 * <p>
 * Think of small helper functions. An example could be a function that sums up
 * all line items to determine the total. It is pure computation based on
 * the given input order argument.
 * <p>
 * Domain Functions can be used by Domain Repositories and Domain Services.
 * However, such functions should not be used cross border. Object and domain
 * services have to have their own set of Domain Functions.
 * <p>
 * Having a limited functionality and being pure accounts for great testability.
 * Usually no mocks are required. Simple, straight unit tests are sufficient
 * most of the time.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(METHOD)
public @interface DomainFunction {
}
