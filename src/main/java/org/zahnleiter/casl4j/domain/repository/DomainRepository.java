package org.zahnleiter.casl4j.domain.repository;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Domain Repositories are used by the domain logic to consume or produce domain
 * entities. Their function is to abstract away REST APIs, messaging and
 * databases. Domain Repositories are just interfaces that use Domain Entities
 * as parameters and return types.
 * <p>
 * Concrete implementations have to be provided to implement interaction with
 * real technologies. For example, an implementation may use Spring JPA
 * repositories and {@code @Entity} if data is to be persisted in a relational
 * database. Another implementation of that same domain repository may use a
 * MongoDB repository and {@code @Document} instead.
 * <p>
 * Domain Repositories are adapters that allow the domain logic to interact with
 * the outside without having to know about technologies. This allows for two
 * things:
 * <ul>
 * <li>Reuse</li>
 * <li>Testability</li>
 * </ul>
 * Implementations must not implement business logic. Rather, they provide basic
 * CRUD functionality.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(TYPE)
public @interface DomainRepository {
}
