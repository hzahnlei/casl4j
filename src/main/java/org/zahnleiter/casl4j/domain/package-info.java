/**
 * This package contains all code that represents the domain logic (aka business
 * logic).
 * <p>
 * The domain logic can be characterized as follows:
 * <ol>
 * <li>It is universally applicable. It may be reused by different (micro)
 * services within the domain. And therefore it may be developed and packaged
 * separately from a concrete service/application.</li>
 * <li>It is free of (technical) frameworks and technology. Therefore it is
 * easy to test. Simple unit and more ambitious component tests are required.
 * But no integration tests are required.</li>
 * <li>This is usually the biggest part of the software and requires extensive
 * testing to make sure the business logic is correct.</li>
 * </ol>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.domain;