package org.zahnleiter.casl4j.domain.usecase;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Contains all the input a use case needs to perform its job. Use Case Requests
 * are sort of envelope data structures holding Domain Objects. The may also
 * hold additional meta data.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface UseCaseRequest {
}
