package org.zahnleiter.casl4j.domain.usecase;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Making code transactional due to an annotation probably works only when the
 * annotated method is invoked on a managed bean, for example a Spring AOP
 * Proxy.
 * <p>
 * Annotations are certainly convenient, therefore this library provides one.
 * However, programmatic transaction control gives the developer more control,
 * which is required sometimes. Therefore {@link AbstractBusinessTransaction} is
 * provided.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(METHOD)
public @interface BusinessTransaction {
}
