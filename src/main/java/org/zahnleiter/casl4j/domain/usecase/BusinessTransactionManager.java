package org.zahnleiter.casl4j.domain.usecase;

/**
 * A technology agnostic transaction manager. Concrete implementations need to
 * be derived, for example based on Spring Framework.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface BusinessTransactionManager {

    /**
     * @return A new transaction object. Has to be committed in case of success or
     *         rolled back in case of error.
     */
    AbstractBusinessTransaction beginNewBusinessTransaction();

}
