package org.zahnleiter.casl4j.domain.usecase;

/**
 * A technology agnostic transaction. Concrete implementations need to be
 * derived, for example based on Spring Framework.
 * <p>
 * Alternatively the {@link BusinessTransaction} annotation can be used, if no
 * fine-grained transaction control is required.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public abstract class AbstractBusinessTransaction implements AutoCloseable {

    @Override
    public final void close() throws Exception {
        rollback();
    }

    /**
     * Commits a transaction so that it has a persistent effect.
     * Does nothing if transaction has been commited or rolled back already.
     */
    public final void commit() {
        if (needsCompletion()) {
            doCommit();
        }
    }

    /**
     * Implement the actual commit here.
     */
    protected abstract void doCommit();

    /**
     * Rolls back a transaction so that it has no effect.
     * Does nothing if transaction has been commited or rolled back already.
     */
    public final void rollback() {
        if (needsCompletion()) {
            doRollback();
        }
    }

    /**
     * Implement the actual roll back here.
     */
    protected abstract void doRollback();

    /**
     * @return {@code true} if transaction has neither been rolled back nor been
     *         commited yet. {@code false} else.
     */
    public abstract boolean needsCompletion();

}
