package org.zahnleiter.casl4j.domain.usecase;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Use Cases are the most complex form of business logic.
 * <p>
 * Use Cases are orchestrating multiple (at least one) Domain Services to
 * achieve a more complex piece business logic in order to support business
 * applications.
 * <p>
 * Use Cases also have the knowledge about transactions. Where should a
 * transaction start, and where should it end? What are the transaction
 * boundaries?
 * <p>
 * Like Domain Services, Use Cases are harder to unit test. More likely they
 * require mocking and extensive component testing to ensure correct business
 * logic.
 * <p>
 * Use Cases might be very application specific and not generally reusable in
 * other scenarios. In this case I would place them in the {@code application}
 * package. This is often the case when building backends for frontends. Such
 * backends are totally application specifc.
 * <br>
 * However, there might very well be Use Cases that are universal and therefore
 * reusable. In this case I would place them here in the {@code domain} package.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface UseCase {
}
