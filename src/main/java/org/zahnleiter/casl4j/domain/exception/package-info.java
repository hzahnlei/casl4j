/**
 * Derivatives of
 * {@link org.zahnleiter.casl4j.domain.exception.AbstractDomainException}
 * are placed in this package.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.domain.exception;