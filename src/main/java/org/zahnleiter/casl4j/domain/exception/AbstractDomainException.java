package org.zahnleiter.casl4j.domain.exception;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * An exception to signal that something went wrong in the business logic.
 * This represents functional exceptions.
 * <ul>
 * <li>Most likely cause by the user (erroneous or incomplete input).</li>
 * <li>Cannot be healed by software automatically.</li>
 * <li>Can be healed by the user, for example by correcting the input. Therefore
 * it makes sense to display full details to the user. ID and time of occurrence
 * should be displayed as well. If the user does not know how to fix the
 * problem, then he/she might request support. In that case ID and time might
 * help with the analysis.</li>
 * </ul>
 * Exceptions should always have a message to provide information about what
 * caused it. This exception does not leave us a choice whether to pass a
 * message or not.
 * <p>
 * This exception also carries a UUID and a time-stamp such that we can better
 * trace the exception (across system boundries).
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public abstract class AbstractDomainException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * ID uniquely identifying a specific occurrence of this exception. Supposed to
     * assist in the search for corresponding log entries.
     */
    public final UUID id;

    /**
     * Date and time at which this specific error occurred. Supposed to assist in
     * the search for corresponding log entries.
     */
    public final OffsetDateTime timeOfOccurrence;

    /**
     * That error has just happened. There is no other exception that had caused
     * this.
     *
     * @param message          Succintly explain what has gone wrong.
     * @param id               See {@link #id}
     * @param timeOfOccurrence See {@link #timeOfOccurrence}
     */
    protected AbstractDomainException(final String message, final UUID id, final OffsetDateTime timeOfOccurrence) {
        super(message);
        this.id = id;
        this.timeOfOccurrence = timeOfOccurrence;
    }

    /**
     * That error has been caused by an error that has happened before.
     *
     * @param message          Succintly explain what has gone wrong.
     * @param cause            The original error.
     * @param id               See {@link #id}
     * @param timeOfOccurrence See {@link #timeOfOccurrence}
     */
    protected AbstractDomainException(final String message, final Exception cause, final UUID id,
            final OffsetDateTime timeOfOccurrence) {
        super(message, cause);
        this.id = id;
        this.timeOfOccurrence = timeOfOccurrence;
    }

}
