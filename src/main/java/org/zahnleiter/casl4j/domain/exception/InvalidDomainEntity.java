package org.zahnleiter.casl4j.domain.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Thrown when a Domain Entity is found to be invalid.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class InvalidDomainEntity extends AbstractDomainException {

    private static final long serialVersionUID = 1L;

    /**
     * Indicates that one has attempted to create a Domain Entity but providing
     * erroneous values or maybe mandataory data was missing.
     *
     * @param message          Explain what field is invalid and why.
     * @param id               See {@link AbstractDomainException#id}
     * @param timeOfOccurrence See {@link AbstractDomainException#timeOfOccurrence}
     */
    public InvalidDomainEntity(final String message, final UUID id, final OffsetDateTime timeOfOccurrence) {
        super(format("Invalid domain entity. %s", message), id, timeOfOccurrence);
    }

}
