package org.zahnleiter.casl4j.domain.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Thrown when a Domain Type is found to be invalid.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class InvalidValueForDomainType extends AbstractDomainException {

    private static final long serialVersionUID = 1L;

    /**
     * Indicates that one has attempted to create a Domain Type but providing a
     * erroneous value or maybe omitting the value overall.
     *
     * @param message          Explain what is missing or why the given value is
     *                         wrong.
     * @param id               See {@link AbstractDomainException#id}
     * @param timeOfOccurrence See {@link AbstractDomainException#timeOfOccurrence}
     */
    public InvalidValueForDomainType(final String message, final UUID id, final OffsetDateTime timeOfOccurrence) {
        super(format("Invalid field value. %s", message), id, timeOfOccurrence);
    }

}
