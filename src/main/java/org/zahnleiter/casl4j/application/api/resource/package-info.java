/**
 * Resources are data transportation objects (DTO) meant to be sent/received via
 * REST APIs.
 * <p>
 * We should distinguish between the resources we present at our API and the
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s we use in our
 * business logic. This way we decouple API from business logic.
 * <ul>
 * <li>Resources can be optimized for the wire.</li>
 * <li>{@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s can be
 * optimized for our business logic.</li>
 * <li>Changes to the business logic do not necessarily change the API and vice
 * versa.</li>
 * </ul>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.api.resource;
