/**
 * Clients for consuming other APIs.
 * <p>
 * Usually a {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}
 * implementation using a generated or custom-built client.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.api.client;
