/**
 * Controllers for exposing this application's/service's API.
 * <p>
 * In case of code generation (openAPI aka Swagger) this might actually not
 * contain controllers but delegates.
 * <p>
 * There is a 1:1 relationship between method/path and Use Case. There has to be
 * a dedicated Use Case to support a method/path combination. Or the opposite
 * around, each Use Case is to be exposed on a separate method/path.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.api.controller;
