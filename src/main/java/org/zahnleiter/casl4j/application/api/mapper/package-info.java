/**
 * This package hosts mapper code to map incoming data transportation objects to
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s. It also contains
 * mapper code to map outgoing
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s to DTOs.
 * <p>
 * DTO is a common term for object send around between services. In case of REST
 * APIs these are wire representations of the resources managed by the API.
 * <p>
 * Mapping can be done automatically by applying mapping frameworks. In that
 * case this package might be omitted.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.api.mapper;
