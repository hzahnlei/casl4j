/**
 * This package typically hosts REST controllers and REST clients. But GraphQL
 * controllers and RPC code could be placed here as well.
 * <p>
 * The controllers are exposing
 * {@link org.zahnleiter.casl4j.domain.usecase.UseCase}s for being consumed
 * remotely by web applications and other services.
 * <p>
 * The clients are implementations of
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}s that allow
 * sending to/receiving from other APIs.
 * <p>
 * Also, we need to map
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s
 * to data transportation objects and vice versa.
 * <p>
 * When possible we generate clients/servers etc. from openAPI specifications.
 * So the implementation contained herein are delegates (servers) or simple
 * wrappers (clients).
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.api;
