/**
 * Custom code to map {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}
 * to entities/documents and vice versa.
 * <p>
 * This package might be omitted if mapping frameworks are used.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence.mapper;
