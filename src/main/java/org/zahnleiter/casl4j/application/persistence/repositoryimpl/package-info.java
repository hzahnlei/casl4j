/**
 * Herein we keep the implementations of the
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}s.
 * <p>
 * The job of the implementations is:
 * <ul>
 * <li>Mapping between {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s
 * and entities/documents.</li>
 * <li>Actually storing/retrieving data by means of Spring repositories, JDBC
 * templates and the like.</li>
 * </ul>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence.repositoryimpl;
