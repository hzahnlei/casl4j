/**
 * Here we find implementations of
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}s for
 * database access. Databases may be rational, graph, key value etc. We are not
 * restricted to SQL databases.
 * <p>
 * Also, we need to map
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s
 * to entities/documents and vice versa.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence;
