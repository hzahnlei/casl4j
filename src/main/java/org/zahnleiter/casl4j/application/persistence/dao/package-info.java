/**
 * Low level Data Access Objects (DAO) reside herein. In case of the Spring
 * framework this might be repositories or JDBC templates etc.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence.dao;
