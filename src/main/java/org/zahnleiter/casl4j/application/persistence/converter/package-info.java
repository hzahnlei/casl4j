/**
 * Converters are used for low level transformation of Java types to types
 * understood by the database. For example, Java's
 * {@link java.time.OffsetDateTime} might require a mapping to a SQL timestamp
 * type.
 * <p>
 * However, such conversion may not be required and in that case this package
 * will be omitted.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence.converter;
