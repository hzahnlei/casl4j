/**
 * A collection of entities/documents to be managed by the database. If we use
 * Spring Data for example then objects to be stored are named:
 * <ul>
 * <li>Entity - when stored in an SQL database (JPA repository)</li>
 * <li>Document - when stored in a NoSQL database (Mongo repository for
 * example)</li>
 * </ul>
 * <p>
 * We should distinguish between the entities/documents we store/retrieve and
 * the {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s we use in our
 * business logic. This way we decouple our persistence from business logic.
 * <ul>
 * <li>Entities/documents can be optimized for the storage/retrieval.</li>
 * <li>{@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s can be
 * optimized
 * for our business logic.</li>
 * <li>Changes to the business logic do not necessarily change the persistence
 * and vice versa.</li>
 * </ul>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.persistence.entity;
