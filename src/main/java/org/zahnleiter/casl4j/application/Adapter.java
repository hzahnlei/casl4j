package org.zahnleiter.casl4j.application;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Concrete implementations of abstractions can be annotated so that we can
 * easily identify them. A typical example would be an implementation of an
 * abstract Domain Repository based on a Spring JPA repository.
 * <p>
 * Clean Architecture Support Library for Java
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Retention(CLASS)
@Target(TYPE)
public @interface Adapter {
}
