/**
 * This package contains Use Cases that are not universal and therefore not
 * reusable in other applications. This is often the case when building backends
 * for frontends. But it also applies to Use Cases that are more technical than
 * functional. Examples might be "self-service password reset", "create shop
 * account" etc.
 * <p>
 * In general I recommend building backends for frontends by using tools such as
 * KrakenD. Configure these tools instead of programming them. (Less code - less
 * bugs.)
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.usecase;
