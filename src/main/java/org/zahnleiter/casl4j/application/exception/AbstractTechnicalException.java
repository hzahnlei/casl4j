package org.zahnleiter.casl4j.application.exception;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * We are just building business applications on top of frameworks, inside a
 * servlet engine or application server. We are not handling threads and sockets
 * directly. Hence, we are not producing technical exceptions on our own.
 * However, we may decide to catch a technical exception
 * and wrap it in our traceable exception to provide more details for logging
 * and metrics.
 * <ul>
 * <li>Technical exceptions might be handled by and healed by the program
 * automatically for example by retrying to establish a network connection.</li>
 * <li>Showing details to the user most likely makes no sense. The user cannot
 * do anything about it. However, a user can report ID and time of occurrence to
 * the support team. ID and time may help finding related log entries and
 * ultimately in identifying and solving the problem.</li>
 * </ul>
 *
 * This exception also carries a UUID and a time-stamp such that we can better
 * trace the exception (across system boundries).
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public abstract class AbstractTechnicalException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * ID uniquely identifying a specific occurrence of this exception. Supposed to
     * assist in the search for corresponding log entries.
     */
    public final UUID id;

    /**
     * Date and time at which this specific error occurred. Supposed to assist in
     * the search for corresponding log entries.
     */
    public final OffsetDateTime timeOfOccurrence;

    /**
     * That error has just happened. There is no other exception that had caused
     * this.
     *
     * @param message          Succintly explain what has gone wrong.
     * @param id               See {@link #id}
     * @param timeOfOccurrence See {@link #timeOfOccurrence}
     */
    protected AbstractTechnicalException(final String message, final UUID id, final OffsetDateTime timeOfOccurrence) {
        super(message);
        this.id = id;
        this.timeOfOccurrence = timeOfOccurrence;
    }

    /**
     * That error has been caused by an error that has happened before.
     *
     * @param message          Succintly explain what has gone wrong.
     * @param cause            The original error.
     * @param id               See {@link #id}
     * @param timeOfOccurrence See {@link #timeOfOccurrence}
     */
    protected AbstractTechnicalException(final String message, final Exception cause, final UUID id,
            final OffsetDateTime timeOfOccurrence) {
        super(message, cause);
        this.id = id;
        this.timeOfOccurrence = timeOfOccurrence;
    }

}
