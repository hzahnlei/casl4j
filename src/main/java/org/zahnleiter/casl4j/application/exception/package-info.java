/**
 * Technical exceptions are usually not thrown by our code as we are writing
 * business applications not compilers, operating systems or servlet engines.
 * Technical aspects are usually handled by frameworks we are using.
 * <br>
 * However, we may catch and wrap technical exceptions to add more context.
 * <p>
 * Derivatives of
 * {@link org.zahnleiter.casl4j.application.exception.AbstractTechnicalException}
 * might be placed in this package.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.exception;
