/**
 * This package is accommodating all (technical) application logic.
 * <p>
 * Application logic can be characterized as follows:
 * <ol>
 * <li>It is tied to a specific application, that makes it not as universally
 * applicable as the more general business logic.</li>
 * <li>It represents the technical part of a (micro) service. It uses frameworks
 * and technologies and therefore is harder to test. Integration tests are
 * required to ensure it is working correctly.</li>
 * </ol>
 * In addition to the application logic, DTOs and such things, one will also
 * find the adapter implementations for our abstractions from our {@code domain}
 * package herein.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application;
