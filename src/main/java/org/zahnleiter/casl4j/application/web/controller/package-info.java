/**
 * Web controllers to deliver web content are residing here.
 * It is unlikely that this package is ever used as the majority of web
 * applications are built as single page web apps with JavaScript and one of the
 * popular web frameworks.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.web.controller;
