/**
 * Here we find everything required for serving web pages. However, in the era
 * of single page web apps it is likely we do not need this package. Instead,
 * our web front-end will consume our API as exposed in the API package.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.web;
