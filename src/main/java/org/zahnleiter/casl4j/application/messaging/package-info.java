/**
 * This package hosts everything related to messaging technologies. This could
 * be Spring integration flows, AMQP templates or gateways etc.
 * <p>
 * Herein we may find implementations of
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository}s to
 * send/receive Domain Entities by means of messaging.
 * <p>
 * Also, we need to map
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s
 * to message objects and vice versa.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.messaging;
