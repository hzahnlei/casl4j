/**
 * Messages are data transportation objects (DTO) intended to be received or
 * sent by means of a messaging infrastructure.
 * <p>
 * We should distinguish between the messages we send/receive and the
 * {@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s we use in our
 * business logic. This way we decouple our messaging from business logic.
 * <ul>
 * <li>Messages can be optimized for the wire.</li>
 * <li>{@link org.zahnleiter.casl4j.domain.entity.DomainEntity}s can be
 * optimized
 * for our business logic.</li>
 * <li>Changes to the business logic do not necessarily change the messaging and
 * vice versa.</li>
 * </ul>
 *
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.messaging.message;
