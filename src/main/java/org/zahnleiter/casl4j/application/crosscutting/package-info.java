/**
 * Cross cutting functions should be located here. For example: Spring AOP can
 * be used to amend business logic with the ability to send business events
 * without mixing our business logic with technical knick-knack.
 * <p>
 * Depending on your needs additional sub-packages might be created such as:
 * {@code metrics}, {@code audit}, {@code security}, {@code logging} etc.
 * <p>
 * Remember, we try to keep the business logic free from technology. Also we
 * want to keep it free from cross-cutting stuff in order to keep our business
 * logic readable/understandable and easy to test.
 * <p>
 * AOP is an interesting way of getting things done. However, sometimes explicit
 * function calls are easier to understand. You can also use the
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository} abstraction
 * to keep your {@link org.zahnleiter.casl4j.domain.service.DomainService} free
 * from technology by explicitly call cross-cutting functions. At the level of
 * {@link org.zahnleiter.casl4j.domain.usecase.UseCase}s you can use the
 * {@link org.zahnleiter.casl4j.domain.service.DomainService} abstraction to
 * achieve the same. In both cases the cross-cutting functions are then provided
 * by implementations of
 * {@link org.zahnleiter.casl4j.domain.repository.DomainRepository} or
 * {@link org.zahnleiter.casl4j.domain.service.DomainService}, located herein.
 * <p>
 * Clean Architecture Support Library for Java
 * <br>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
package org.zahnleiter.casl4j.application.crosscutting;
