# Change Log

## 1.0.0 - Initial version (2022-05-26)

### Functional

-  Basic classes and annotations
-  Quick introduction to clean architecture
-  Proposal for naming convention
-  Proposal for Java package structure

### Non-Functional

-  Gradle build script
-  Publish module to GitLab Maven repository
-  Produce coverage report and push to GitLab for display and merge requests
-  Push test report to GitLab for display
